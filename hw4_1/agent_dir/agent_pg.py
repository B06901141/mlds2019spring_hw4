from agent_dir.agent import Agent
import scipy
import scipy.misc

import tqdm

import numpy as np
import sys

import tensorflow as tf
import keras

import gc
#sess = keras.backend.get_session()
"""
import matplotlib
matplotlib.use("Agg")
import matplotlib.pyplot as plt
"""


class Norm(keras.layers.Layer):
    def call(self, x):
        return tf.transpose((tf.transpose(x)/tf.transpose(tf.reduce_sum(x, axis=1))))

    def compute_output_shape(self, input_shape):
        return input_shape


def prepro(o, image_size=[80, 80]):
    """
    Call this function to preprocess RGB image to grayscale image if necessary
    This preprocessing code is from
        https://github.com/hiwonjoon/tf-a3c-gpu/blob/master/async_agent.py

    Input: 
    RGB image: np.array
        RGB screen of game, shape: (210, 160, 3)
    Default return: np.array 
        Grayscale image, shape: (80, 80, 1)

    """
    o = o[34:194, :, :] / 255
    y = 0.2126 * o[:, :, 0] + 0.7152 * o[:, :, 1] + 0.0722 * o[:, :, 2]
    #y = y.astype(np.uint8)
    y = scipy.misc.imresize(y, size=image_size)
    return y


class Baseline(keras.callbacks.Callback):
    def __init__(self, baseLine):
        self.baseLine = baseLine

    def on_epoch_end(self, epoch, logs={}):
        if logs['loss'] < self.baseLine:
            self.model.stop_training = True
            self.model.set_weights(self.weights)

    def on_epoch_begin(self, epoch, logs={}):
        self.weights = self.model.get_weights()


class Agent_PG(Agent):
    def __init__(self, env, args):
        """
        Initialize every things you need here.
        For example: building your model
        """

        super(Agent_PG, self).__init__(env)

        #self.output_dim = env.get_action_space().n
        self.learning_rate = args.learning_rate
        self.epochs = args.epochs
        self.decay_rate = args.decay_rate
        self.init_screen = self.env.reset()
        self.action_list = [2, 3]
        self.action2index = {j: i for i, j in enumerate(self.action_list)}
        self.is_compile = False
        if args.test_pg:
            # you can load your model here
            print('loading trained model')
        else:
            init = keras.initializers.he_normal(seed=None)
            input_ = keras.layers.Input(shape=(80, 80))
            output = input_
            output = keras.layers.Flatten()(output)
            #output = keras.layers.Dense(256)(output)
            #output = keras.layers.LeakyReLU(alpha = 0.3)(output)
            output = keras.layers.Dense(10,activation = 'relu')(output)
            #output = keras.layers.LeakyReLU(alpha=0.3)(output)
            output = keras.layers.Dense(
                len(self.action_list), activation="softmax")(output)
            self.model = keras.models.Model(input_, output)
            # self.compile(opt)
            self.model.summary()

    def compile(self, opt, loss='sparse_categorical_crossentropy'):
        """
        if not self.is_compile:
            self.is_compile = True
            self.reward_placeholder = keras.backend.placeholder(shape = (None,))
            self.action_placeholder = keras.backend.placeholder(shape = (None,),dtype = tf.int64)
            label = keras.backend.one_hot(self.action_placeholder,len(self.action_list))
            prob = keras.backend.sum(label*self.model.output,axis = 1)

            action_log_prob = keras.backend.log(prob + 1e-14)
            self.loss = (- tf.reduce_mean(action_log_prob * self.reward_placeholder) + 100000*tf.reduce_mean(self.model.layers[-1].kernel ** 2)
                + 100000*tf.reduce_mean(self.model.layers[-3].kernel ** 2) + 100000*tf.reduce_mean(self.model.layers[-5].kernel ** 2))
            correct_prediction = tf.cast(tf.equal(self.action_placeholder,tf.argmax(self.model.output,axis = 1)),tf.float32)
            self.acc = tf.reduce_mean(correct_prediction)
        updates = opt.get_updates(params=self.model.trainable_weights,loss=self.loss)
        self.train_fn = keras.backend.function(inputs=[self.model.input,self.action_placeholder,self.reward_placeholder],
                               outputs=[self.loss,self.acc],
                               updates=updates)
        """
        self.model.compile(loss=loss, optimizer=opt, metrics=["accuracy"])

    def preprocessR(self, r):
        result = np.zeros_like(r, dtype=np.float32)
        running_add = 0
        for i in range(result.shape[0]-1, -1, -1):
            if r[i] != 0:
                running_add = r[i]
            result[i] = running_add
            # if running_add < 0:
            running_add *= self.decay_rate
        #result -= (result.mean())
        result /= (result.std())
        return result

    def init_game_setting(self):
        """

        Testing function will call this function at the begining of new game
        Put anything you want to initialize if necessary

        """
        ##################
        # YOUR CODE HERE #
        ##################
        pass

    def train(self):
        """
        Implement your training algorithm here
        """
        with open("history.csv", "w") as file1:
            file1.write("episode,reward\n")
        #r_history = []
        step = 10
        mse_baseline = 0.005
        pretrain = True
        r_history = []
        #total_reward_history = []
        batch_size = 32
        for start in range(1, self.epochs + 1, step):
            end = start + step
            total_reward = 0
            total_A = []
            total_S = []
            total_R = []
            for episode in range(start, end):
                self.init_screen = self.env.reset()
                S = [self.init_screen]
                A = []
                R = []
                done = False
                reward = 0
                s = self.init_screen
                while not done:
                    """
                    if episode <= threshold:
                        p = np.ones_like(self.action_list)/len(self.action_list)
                        a = np.random.choice(self.action_list,p=p)
                    else:
                    """
                    a = self.make_action(s)
                    self.init_screen = s
                    s, r, done, info = self.env.step(a)
                    reward += r
                    S.append(self.init_screen)
                    A.append(a)
                    R.append(r)
                S = np.array([prepro(i) for i in S])
                S = np.array([S[i+1]-S[i] for i in range(len(S)-1)])
                A = np.array([self.action2index[i] for i in A])
                R = self.preprocessR(np.array(R))
                S = S[1:]
                A = A[1:]
                R = R[1:]
                total_A.extend(A)
                total_S.extend(S)
                total_R.extend(R)
                print("Episode %d/%d, reward=%d" %
                      (episode, self.epochs, reward))
                with open("history.csv", "a") as file1:
                    file1.write("%d,%d\n" % (episode, reward))
                total_reward += reward
            total_reward /= step
            total_S = np.array(total_S)
            total_A = np.array(total_A)
            total_R = np.array(total_R)
            r_history.append(np.mean(total_R))
            if not pretrain:
                total_R -= np.mean(r_history)
            if pretrain:
                total_R = np.ones_like(total_R)
                total_A = np.ones((total_A.shape[0], len(
                    self.action_list))) / len(self.action_list)
            loss = "categorical_crossentropy" if pretrain else "sparse_categorical_crossentropy"
            metrics = ["mean_squared_error"] if pretrain else ["acc"]
            opt = keras.optimizers.Adam(
                lr=self.learning_rate, beta_1=0.9, beta_2=0.999, epsilon=None, decay=0.0, amsgrad=False)
            baseline = Baseline(-1)
            self.model.compile(loss=loss, optimizer=opt, metrics=metrics)
            history = self.model.fit(total_S, total_A, batch_size=batch_size,
                                     epochs=step*2, sample_weight=total_R, verbose=1, callbacks=[baseline])
            loss = history.history["loss"][-1]
            metric = history.history[metrics[0]][-1]
            # if loss > -1:
            print("Updated, Episode %d/%d, loss=%.4f, %s=%.4f, reward=%4.1f" %
                  (episode, self.epochs, loss, metrics[0], metric, total_reward))
            # else:
            #print("Discard the result of this update. reward=%4.1f"%total_reward)
            # r_history.pop()
            #self.model = keras.models.load_model("model.hdf5")
            if pretrain and metric < mse_baseline:
                pretrain = False
            self.model.save("model.hdf5")
            if episode % (10*step) == 0:
                keras.backend.clear_session()
                gc.collect()
                self.model = keras.models.load_model("model.hdf5")
            #R = -np.ones_like(R)
            # print(A)
            """
            batch_num = A.shape[0] // self.batch_size + int(A.shape[0] % self.batch_size != 0)
            bar = tqdm.trange(batch_num,ncols = 100)
            description = "Episode %d/%d"%(episode,self.epochs)
            bar.set_description(description)
            losses = []
            accs = []
            for batch in bar:
                bar.refresh()
                S_batch = S[batch*self.batch_size:(batch+1)*self.batch_size]
                A_batch = A[batch*self.batch_size:(batch+1)*self.batch_size]
                R_batch = R[batch*self.batch_size:(batch+1)*self.batch_size]
                loss,acc = self.train_fn([S_batch,A_batch,R_batch])
                postfix = {'loss':loss,'acc':acc,'reward':reward}
                losses.append(loss)
                accs.append(acc)
                bar.set_postfix(postfix)
            loss = np.mean(losses)
            acc = np.mean(accs)
            postfix = {'loss':loss,'acc':acc,'reward':reward}
            bar.set_postfix(postfix)
            bar.close()
            """
            # r_history.append(R.mean())
            #baseLine = np.mean(r_history)

    def make_action(self, observation, test=True):
        """
        Return predicted action of your agent

        Input:
            observation: np.array
                current RGB screen of game, shape: (210, 160, 3)

        Return:
            action: int
                the predicted action from trained model
        """
        observation = prepro(observation) - prepro(self.init_screen)
        observation = np.expand_dims(observation, axis=0)
        prob = self.model.predict(observation)[0]
        return np.random.choice(self.action_list, p=prob)
