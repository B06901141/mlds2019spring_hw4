def add_arguments(parser):
    '''
    Add your arguments here if needed. The TAs will run test.py to load
    your default arguments.

    For example:
        parser.add_argument('--batch_size', type=int, default=32, help='batch size for training')
        parser.add_argument('--learning_rate', type=float, default=0.01, help='learning rate for training')
    '''
    parser.add_argument('--learning_rate', type=float,
                        default=0.001, help='learning rate for training')
    parser.add_argument('--epochs', type=int, default=20000,
                        help='epochs for training')
    parser.add_argument('--decay_rate', type=float,
                        default=0.99, help='decay rate for reward')
    return parser
