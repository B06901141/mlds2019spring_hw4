from __future__ import absolute_import, division

import os
from os import path as os_path

import cv2
import gym
import numpy as np
import torch
from six.moves import range
from torch import cuda
from torch import distributions as P
from torch import nn, optim
from torch.nn import functional as F

#device = "cuda" if cuda.is_available() else "cpu"
device = "cpu"


def vae_loss(recon_x, x, mu, logvar):
    BCE = F.binary_cross_entropy(recon_x, x, reduction='sum')
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
    return BCE + KLD


def policy_gradient(agent, agent_optim, observation, action, reward, decay):
    '''called after an episode
    '''

    R_total = 0
    G = []

    _reward = list(reward)

    for R_step in _reward[::-1]:
        R_total = R_step + R_total*decay
        G.insert(0, R_total)

    total_loss = 0
    for step in range(len(action)):
        prediction = agent(observation[step:step+1])
        a = action[step:step+1]
        loss = F.cross_entropy(prediction, a)*G[step]
        total_loss = total_loss + loss
    agent_optim.zero_grad()
    total_loss.backward()
    agent_optim.step()


class Reshape(nn.Module):
    def __init__(self, *shapes):
        super(Reshape, self).__init__()
        self.shapes = shapes

    def forward(self, x):
        return x.view(len(x), *self.shapes)


class ConvVAE(nn.Module):
    def __init__(self, latent):
        super(ConvVAE, self).__init__()
        self.inconv = nn.Sequential(
            nn.Conv2d(in_channels=3,
                      out_channels=32,
                      kernel_size=4,
                      stride=2),
            nn.ReLU(),
            nn.Conv2d(in_channels=32,
                      out_channels=64,
                      kernel_size=4,
                      stride=2),
            nn.ReLU(),
            nn.Conv2d(in_channels=64,
                      out_channels=128,
                      kernel_size=4,
                      stride=2),
            nn.ReLU(),
            nn.Conv2d(in_channels=128,
                      out_channels=256,
                      kernel_size=4,
                      stride=2),
            nn.ReLU(),
            Reshape(1024),
        )
        self.mu = nn.Linear(in_features=1024, out_features=latent)
        self.sigma = nn.Linear(in_features=1024, out_features=latent)
        self.dense = nn.Linear(in_features=latent, out_features=1024)
        self.outconv = nn.Sequential(
            Reshape(1024, 1, 1),
            nn.ConvTranspose2d(in_channels=1024,
                               out_channels=128,
                               kernel_size=5,
                               stride=2),
            nn.ReLU(),
            nn.ConvTranspose2d(in_channels=128,
                               out_channels=64,
                               kernel_size=5,
                               stride=2),
            nn.ReLU(),
            nn.ConvTranspose2d(in_channels=64,
                               out_channels=32,
                               kernel_size=6,
                               stride=2),
            nn.ReLU(),
            nn.ConvTranspose2d(in_channels=32,
                               out_channels=3,
                               kernel_size=6,
                               stride=2),
            nn.Sigmoid()
        )

    def encode(self, x):
        assert (x <= 1).all() and (x >= 0).all()
        x = self.inconv(x)
        mu, logvar = self.mu(x), self.sigma(x)
        std = torch.exp(.5*logvar)
        noise = torch.randn_like(std).to(device)
        std = std * noise
        x = mu + std
        return x, (mu, logvar)

    def decode(self, x):
        x = self.dense(x)
        x = self.outconv(x)
        return x

    def forward(self, x):
        x, (mu, logvar) = self.encode(x)
        x = self.decode(x)
        return x, mu, logvar


class RandomAgent(nn.Module):
    def __init__(self, limit, *args, **kwargs):
        super(RandomAgent, self).__init__()
        self.limit = limit

    def forward(self, observation):
        return torch.randn(self.limit)


if __name__ == "__main__":
    latent = 64*64*3
    hidden = 2000
    vae_iters = 0
    agent_iters = 20000
    gamma = .9

    env = gym.make("Pong-v0")
    os.makedirs("state_dicts", exist_ok=True)

    actions = env.action_space.n

    agent = nn.Sequential(
        nn.Linear(in_features=latent,
                  out_features=hidden),
        nn.Tanh(),
        nn.Linear(in_features=hidden,
                  out_features=hidden),
        nn.Tanh(),
        nn.Linear(in_features=hidden,
                  out_features=actions)
    ).to(device)
    agentopt = optim.Adam(agent.parameters())
    random_agent = RandomAgent(limit=actions)

    def take_action(agent, observation):
        out = agent(observation)
        observation = torch.tensor(out).float().to(device)
        observation = F.softmax(observation, dim=-1)
        m = torch.distributions.Categorical(probs=observation)
        action = m.sample([]).squeeze()
        return action.cpu().numpy()

    def split(A, O, R):
        _A, _O, _R = [], [], []
        stop = 0
        for i in range(len(R)):
            if R[i] != 0:
                _R.append(R[stop:i+1])
                _A.append(A[stop:i+1])
                _O.append(O[stop:i+1])
                stop = i+1
        if stop != i+1:
            _R.append(R[stop:])
            _A.append(A[stop:])
            _O.append(O[stop:])
        return _A, _O, _R

    class dummy:
        def encode(self, x):
            shape = x.shape
            return x.view(1, -1), None

    vae = dummy()
    with open("history.txt", 'w') as file1:
        file1.write("episode,reward\n")
    for i in range(1, 1+agent_iters):
        print("agent episode %04d" % i, end=' ')
        done = False
        observation = env.reset()
        A = []
        im = cv2.resize(observation, (64, 64))/255
        enc, _ = vae.encode(torch.tensor(im).permute(
            2, 0, 1).unsqueeze(0).float().to(device))
        O = [enc]
        R = []
        while not done:
            im = cv2.resize(observation, (64, 64))/255
            enc, _ = vae.encode(torch.tensor(im).permute(
                2, 0, 1).unsqueeze(0).float().to(device))
            action = take_action(agent, enc).item()
            observation, reward, done, _ = env.step(action)
            A.append(action)
            enc, _ = vae.encode(torch.tensor(im).permute(
                2, 0, 1).unsqueeze(0).float().to(device))
            O.append(enc)
            R.append(reward)

        print("reward %03d" % sum(R), end='\n')
        with open("history.txt", 'a') as file1:
            file1.write("%d,%d\n" % (i, sum(R)))
        O = O[: -1]
        A, O, R = split(A, O, R)
        for act, obs, rew in zip(A, O, R):
            act = torch.tensor(act).to(device)
            obs = torch.cat(obs, dim=0).to(device)
            rew = torch.tensor(rew).to(device)
            policy_gradient(agent, agentopt, obs, act, rew, .9)
    print()
    torch.save(agent.state_dict(), os_path.join("state_dicts", "agemt.pt"))
