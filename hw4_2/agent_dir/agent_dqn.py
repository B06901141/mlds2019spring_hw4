### Q_learning model

import multiprocessing as mp
from time import time

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optimizer
from torch.utils.data import DataLoader
from tqdm import tqdm
import torch.multiprocessing as mp

from collections import deque
from time import time

from torchsummary import summary
from agent import Agent

home_dir = ''

class Flatten(nn.Module):
    def __init__(self):
        super(Flatten, self).__init__()

    def forward(self, x):
        shape = torch.prod(torch.tensor(x.shape[1:])).item()
        shape = int(shape)
        return x.view(-1, shape)

class Channel_swap(nn.Module):
    def __init__(self):
        super(Channel_swap, self).__init__()

    def forward(self, x):
        x = torch.transpose(x, 2, 3)
        x = torch.transpose(x, 1, 2)
        return x

class DQN_Model(nn.Module):
    def __init__(self, num_action):
        super().__init__()
        self.num_action = num_action
        # input shape (84, 84, 4)
        self.channel_first = Channel_swap()
        self.conv1 = nn.Conv2d(4, 32, kernel_size=5, padding=2)
        self.batchnorm1 = nn.BatchNorm2d(num_features=32)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=5, padding=2)
        self.batchnorm2 = nn.BatchNorm2d(num_features=64)
        self.maxplool1 = nn.MaxPool2d(kernel_size=2)
        # input shape (64, 42, 42)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=5, padding=2)
        self.batchnorm3 = nn.BatchNorm2d(num_features=64)
        self.conv4 = nn.Conv2d(64, 64, kernel_size=3, padding=0)
        self.batchnorm4 = nn.BatchNorm2d(num_features=64)
        self.maxplool2 = nn.MaxPool2d(kernel_size=2)
        # input shape (64, 20, 20)
        self.conv5 = nn.Conv2d(64, 64, kernel_size=3, padding=0)
        self.batchnorm5 = nn.BatchNorm2d(num_features=64)
        self.conv6 = nn.Conv2d(64, 64, kernel_size=3, padding=0)
        self.batchnorm6 = nn.BatchNorm2d(num_features=64)
        self.maxplool3 = nn.MaxPool2d(kernel_size=2)
        # input shape (64, 8, 8)
        self.flatten = Flatten()
        self.hidden = nn.Linear(64*64, 256)
        self.hidden1 = nn.Linear(256, 256)
        self.output = nn.Linear(256, self.num_action)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.channel_first(x)
        x = self.conv1(x)
        x = self.relu(x)
        x = self.batchnorm1(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.batchnorm2(x)
        x = self.maxplool1(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = self.batchnorm3(x)
        x = self.conv4(x)
        x = self.relu(x)
        x = self.batchnorm4(x)
        x = self.maxplool2(x)
        x = self.conv5(x)
        x = self.relu(x)
        x = self.batchnorm5(x)
        x = self.conv6(x)
        x = self.relu(x)
        x = self.batchnorm6(x)
        x = self.maxplool3(x)
        x = self.flatten(x)
        x = self.hidden(x)
        x = self.relu(x)
        x = self.hidden1(x)
        x = self.relu(x)
        value = self.output(x)
        adventage = self.output(x)
        return value + adventage  - adventage.mean()

      
class Dueling_DQN_Model(nn.Module):
    def __init__(self, num_action):
        super().__init__()
        self.num_action = num_action
        # input shape (84, 84, 4)
        self.channel_first = Channel_swap()
        self.conv1 = nn.Conv2d(4, 32, kernel_size=5, padding=2)
        self.batchnorm1 = nn.BatchNorm2d(num_features=32)
        self.conv2 = nn.Conv2d(32, 64, kernel_size=5, padding=2)
        self.batchnorm2 = nn.BatchNorm2d(num_features=64)
        self.maxplool1 = nn.MaxPool2d(kernel_size=2)
        # input shape (64, 42, 42)
        self.conv3 = nn.Conv2d(64, 64, kernel_size=5, padding=2)
        self.batchnorm3 = nn.BatchNorm2d(num_features=64)
        self.conv4 = nn.Conv2d(64, 64, kernel_size=3, padding=0)
        self.batchnorm4 = nn.BatchNorm2d(num_features=64)
        self.maxplool2 = nn.MaxPool2d(kernel_size=2)
        # input shape (64, 20, 20)
        self.conv5 = nn.Conv2d(64, 64, kernel_size=3, padding=0)
        self.batchnorm5 = nn.BatchNorm2d(num_features=64)
        self.conv6 = nn.Conv2d(64, 64, kernel_size=3, padding=0)
        self.batchnorm6 = nn.BatchNorm2d(num_features=64)
        self.maxplool3 = nn.MaxPool2d(kernel_size=2)
        # input shape (64, 8, 8)
        self.flatten = Flatten()
        self.hidden = nn.Linear(64*64, 256)
        self.hidden1 = nn.Linear(256, 256)
        self.advantage = nn.Linear(256, self.num_action)
        self.value = nn.Linear(256, 1)
        self.relu = nn.ReLU()

    def forward(self, x):
        x = self.channel_first(x)
        x = self.conv1(x)
        x = self.relu(x)
        x = self.batchnorm1(x)
        x = self.conv2(x)
        x = self.relu(x)
        x = self.batchnorm2(x)
        x = self.maxplool1(x)
        x = self.conv3(x)
        x = self.relu(x)
        x = self.batchnorm3(x)
        x = self.conv4(x)
        x = self.relu(x)
        x = self.batchnorm4(x)
        x = self.maxplool2(x)
        x = self.conv5(x)
        x = self.relu(x)
        x = self.batchnorm5(x)
        x = self.conv6(x)
        x = self.relu(x)
        x = self.batchnorm6(x)
        x = self.maxplool3(x)
        x = self.flatten(x)
        x = self.relu(x)
        x = self.hidden(x)
        x = self.relu(x)
        x = self.hidden1(x)
        x = self.relu(x)
        value = self.value(x)
        advantage = self.advantage(x)
        return value + advantage  - advantage.mean()



class A_sample:
    def __init__(self, data):
        self.data = list(data)
        self.data[0] = self.extract_state(data[0])
        self.data[2] = self.extract_state(data[2])

    def extract_state(self, state):
        return state


class GameSample:
    def __init__(self, step_lookahead, k = 10000):
        self.die_reward = 0
        self.state = deque([], maxlen=k)
        self.action = deque([], maxlen=k)
        self.new_state = deque([], maxlen=k)
        self.reward = deque([], maxlen=k)
        self.done = deque([], maxlen=k)
        self.info = deque([], maxlen=k)
        self.wrapped_sample = deque([], maxlen=k)
        self.step_lookahead = step_lookahead
        self.random_sample_rate = 0.2
        self.living_reward = 0.01

        self.selected_state = [[] for _ in range(self.step_lookahead)]
        self.selected_action = [[] for _ in range(self.step_lookahead)]
        self.selected_new_state = [[] for _ in range(self.step_lookahead)]
        self.selected_reward = [[] for _ in range(self.step_lookahead)]
        self.selected_done = [[] for _ in range(self.step_lookahead)]

    def append(self, gameState):
        self.state.append(self.extract_state(gameState[0]))
        self.action.append(gameState[1])
        self.new_state.append(self.extract_state(gameState[2]))
        if len(self.info) > 0 and self.info[-1]['ale.lives'] > gameState[5]['ale.lives']:
            self.reward.append(gameState[3])
        else:
            self.reward.append(gameState[3] + self.living_reward)
        self.done.append(gameState[4])
        self.info.append(gameState[5])
        self.wrapped_sample.append(A_sample(gameState))

    def process_reward(self):
        
      
        for i in range(len(self.info) - 1):
            # print(self.info[i]['ale.lives'], end=' ')
            if self.info[i + 1]['ale.lives'] < self.info[i]['ale.lives']:
                self.reward[i]

    def select_value_sample(self, batch_size):
        value = [i for i in range(len(self.state)) if self.reward[i] != 0 and i >= self.step_lookahead]
        if len(value) == 0:
          return (np.array(self.selected_state), np.array(self.selected_action),  np.array(self.selected_new_state),
                np.array(self.selected_reward), np.array(self.selected_done))
        selected = np.random.choice(value, batch_size)

        self.selected_state = [[] for _ in range(self.step_lookahead)]
        self.selected_action = [[] for _ in range(self.step_lookahead)]
        self.selected_new_state = [[] for _ in range(self.step_lookahead)]
        self.selected_reward = [[] for _ in range(self.step_lookahead)]
        self.selected_done = [[] for _ in range(self.step_lookahead)]

        for i in selected:
            for j in range(self.step_lookahead):
                self.selected_state[j].append(self.state[i - j])
                self.selected_action[j].append(self.action[i - j])
                self.selected_new_state[j].append(self.new_state[i - j])
                self.selected_reward[j].append(self.reward[i - j])
                self.selected_done[j].append(self.done[i - j])

        return (np.array(self.selected_state), np.array(self.selected_action),  np.array(self.selected_new_state),
                np.array(self.selected_reward), np.array(self.selected_done))

    def random_sample(self, batch_size):
        sample = np.random.choice(self.wrapped_sample, batch_size)
        state = [s.data[0]  for s in sample]
        action = [s.data[1]  for s in sample]
        state_1 = [s.data[2]  for s in sample]
        reward = [s.data[3]  for s in sample]
        done = [s.data[4]  for s in sample]
        return (state, action, state_1, reward, done)

    def extract_state(self, state):
        return state


class Agent_DQN(Agent):
    def __init__(self, env, args):
        """
        Initialize every things you need here.
        For example: building your model
        """

        super(Agent_DQN, self).__init__(env)

        ##################
        # YOUR CODE HERE #
        ##################

        self.lr = args.learning_rate
        self.batch_size = args.batch_size
        self.epochs = args.epoch
        self.device = args.device
        self.start = args.start
        self.num_actions = env.get_action_space().n
        self.state_shape = (84, 84, 4)
        self.num_sample = 10
        self.episilon = 1.0
        self.gamma = 0.99
        self.step_lookahead = 10
        self.TLE = 1000
        self.error_count = 0

        if args.test_dqn:
            # you can load your model here
            print('loading trained model')
            self.build_model()
            self.dqn_model.load_state_dict(torch.load(args.filename))
            # self.dqn_model = load_model('dqn_model.hdf5')

        else:
            self.build_model()
            # self.dqn_model.load_state_dict(torch.load(args.filename))
            # self.target.load_state_dict(torch.load(args.filename))
            self.dqn_model.train()

        # self.dqn_model = load_model('model/dqn_model.hdf5')
        self.optimizer = optimizer.RMSprop(
            self.dqn_model.parameters(), lr=self.lr)
        self.samples = GameSample(self.step_lookahead)
        self.x_train = []
        self.y_train = []
        self.env.seed(0)

        self.history = {'mean': []}
        # average the state of four frame

    def build_model(self):
        self.dqn_model = DQN_Model(self.num_actions)
        self.target = DQN_Model(self.num_actions)
        self.target.load_state_dict(self.dqn_model.state_dict())
        self.dqn_model.to(self.device)
        self.target.to(self.device)
        summary(self.dqn_model, input_size=self.state_shape)
        
    def init_game_setting(self):
        """

        Testing function will call this function at the begining of new game
        Put anything you want to initialize if necessary

        """
        ##################
        # YOUR CODE HERE #
        ##################

        pass

    def fit(self, x_train, y_train, batch_size, epochs=1):
        self.dqn_model.train()

        x_train = DataLoader(x_train, batch_size=batch_size)
        y_train = DataLoader(y_train, batch_size=batch_size)

        tl = 0
        self.dqn_model.eval()
        for x, y in zip(x_train, y_train):
            x, y = torch.tensor(x, dtype=torch.float32).to(
                self.device), torch.tensor(y, dtype=torch.float32).to(self.device)
            pred = self.dqn_model(x)
            loss = F.mse_loss(pred, y)
            tl += loss.item()
            self.optimizer.zero_grad()
            loss.backward()
            self.optimizer.step()

        self.dqn_model.eval()
        return tl / len(x_train)

    def train(self):
        """
        Implement your training algorithm here
        """
        ##################
        # YOUR CODE HERE #
        ##################

        t = time()
        for i in range(self.epochs):
            self.sample()
            
            random_sample = self.samples.random_sample(self.batch_size * 16)
            y_train = self.get_y_train(random_sample)
            x_train = random_sample[0]
            loss = self.fit(x_train, y_train, batch_size=self.batch_size)
            # '''
            

            if (i + 1) % 1000 == 0 and i > 0:
                torch.save(self.dqn_model.state_dict(), '%smodel/%ddqn_model.pt' % (home_dir, self.start + i + 1))

            if (i + 1) % 100 == 0 and i > 0:
                self.target.load_state_dict(self.dqn_model.state_dict())

            if (i + 1) % 10 == 0:
                print('On epoch %d, Loss: %.5f, Time usage: %.2f' % (i + 1, loss, time() - t))
                t = time()

    @torch.no_grad()
    def get_y_train(self, random_sample):
        self.target.eval()

        states = DataLoader(random_sample[0])
        next_states = DataLoader(random_sample[2])

        q_sa = []
        q_s1_a1 = []
        for s, ns in zip(states, next_states):
            pred1 = self.dqn_model(s.to(self.device)).cpu().detach().numpy()
            pred2 = self.target(ns.to(self.device)).cpu().detach().numpy()
            q_sa.append(pred1)
            q_s1_a1.append(pred2)

        q_sa = np.concatenate(q_sa, axis=0)
        q_s1_a1 = np.concatenate(q_s1_a1, axis=0)
        target = np.max(q_s1_a1, axis=1)
        for k, action in enumerate(random_sample[1]):
            if not random_sample[4][k]:
                q_sa[k][action] = random_sample[3][k] + self.gamma * target[k]
            else:
                q_sa[action] = random_sample[3][k]

        return q_sa

    @torch.no_grad()
    def sample_one_game(self):
        state = self.env.reset()
        self.init_game_setting()
        done = False
        episode_reward = 0.0
        cur_step = 0
        # playing one game
        while(not done):
            action = self.make_action(state, test=False)
            new_state, reward, done, info = self.env.step(action)
            episode_reward += reward
            self.samples.append(
                (state, action, new_state, reward, done, info))

            state = new_state
            cur_step += 1
            if cur_step > self.TLE:
                break

            if reward != 0:
                cur_step = 0

    @torch.no_grad()
    def sample(self):
        # self.samples = GameSample(self.step_lookahead)
        self.episilon -= (1 - 0.025) / self.epochs

        for _ in range(self.num_sample):
            self.sample_one_game()


    @torch.no_grad()
    def make_action(self, observation, test=True):
        """
        Return predicted action of your agent

        Input:
            observation: np.array
                stack 4 last preprocessed frames, shape: (84, 84, 4)

        Return:
            action: int
                the predicted action from trained model
        """
        ##################
        # YOUR CODE HERE #
        ##################
        # print(observation.shape)

        frame = observation
        frame = np.expand_dims(frame, axis=0)
        frame = torch.tensor(frame, dtype=torch.float32).to(self.device)
        if test:
            pred = self.dqn_model(frame).cpu().numpy()
            # print(pred[0], np.argmax(pred[0]))
            return np.argmax(pred[0])

        r = np.random.random()

        if r > self.episilon:
            pred = self.dqn_model(frame).cpu().numpy()
            return np.argmax(pred[0])
        else:
            # to_choose = set(range(self.num_actions)) - set([act])
            return np.random.choice([1, 2, 3])
