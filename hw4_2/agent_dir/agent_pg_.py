from __future__ import absolute_import, print_function

import cv2
import numpy as np
import torch
import tqdm
from numpy import random
from torch import cuda, nn, optim
from torch import distributions as P
from torch.nn import functional as F


def resize(img, size=(64, 64)):
    return cv2.resize(img, dsize=size, iterpolate=cv2.INTER_CUBIC)


@torch.no_grad()
def _take_action(agent, observation, epsilon=.1):
    observation = torch.from_numpy(observation)
    action = agent(observation)
    rand = P.Uniform(0, 1).sample([len(action)])
    random_mask = rand < epsilon
    random_arg = torch.randint(0, agent.outdim, size=[len(action)])
    max_arg = action.argmax(-1)
    random_element = random_mask.int()*random_arg.int()
    max_element = (~random_mask).int()*max_arg.int()
    return (random_element+max_element).numpy()


def _update(agent, observations, actions, rewards, gamma=.95, optimizer=None):
    if not optimizer:
        optimizer = agent.optimizer
    assert len(actions) == len(rewards) == len(observations)-1
    observations = torch.from_numpy(observations)
    actions = torch.from_numpy(actions)
    for obs, nex, rew, act in zip(observations[:-1], observations[1:], rewards, actions):
        pred = agent(obs)[act]
        next_pred = agent(nex).max(-1)[0]
        loss = F.smooth_l1_loss(pred, rew+next_pred)
        optimizer.zero_grad()
        loss.backward()
        nn.utils.clip_grad_norm_(agent.parameters(), 1)
        optimizer.step()


class Agent(nn.Module):

    def __init__(self, sizes):
        super(Agent, self).__init__()
        ins = sizes[:-1]
        ous = sizes[1:]
        self.outdim = sizes[-1]
        self.layers = nn.ModuleList([
            nn.Linear(in_features=i, out_features=o) for i, o in zip(ins, ous)
        ])
        self.optimizer = optim.Adam(self.parameters(), lr=1e-3)

    def forward(self, x):
        for layer in self.layers:
            x = F.leaky_relu(x)
            x = layer(x)
        return x

    update = _update

    take_action = _take_action


if __name__ == '__main__':
    agent = Agent([2, 3, 4, 5, 6])
    img = np.array(torch.randn(113, 2))
    action = agent.take_action(img, epsilon=.1)
    printv(action)
    observations = np.array(torch.randn(114, 2))
    rewards = list(range(113))
    agent.update(observations, action, rewards)
