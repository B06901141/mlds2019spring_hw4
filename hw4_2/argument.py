def add_arguments(parser):
    '''
    Add your arguments here if needed. The TAs will run test.py to load
    your default arguments.

    For example:
        parser.add_argument('--batch_size', type=int, default=32, help='batch size for training')
        parser.add_argument('--learning_rate', type=float, default=0.01, help='learning rate for training')
    '''
    parser.add_argument('--batch_size', type=int, default=64, help='batch size for training')
    parser.add_argument('--learning_rate', type=float, default=0.001, help='learning rate for training')
    parser.add_argument('--epoch', type=int, default=10000, help='epochs for training')
    parser.add_argument('--device', type=str, default='cuda', help='The runtime device for pytorch')
    parser.add_argument('--start', type=int, default=0, help='epochs before training')
    parser.add_argument('--filename', type=str, default='model/dqn_model.pt', help='Pretrained model')

    return parser
