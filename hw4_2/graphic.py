import matplotlib.pyplot as plt
import numpy as np

def read_csv(filename):
    with open(filename, 'r') as fin:
        data = fin.read().split()
        return [d for d in map(lambda x: float(x.rstrip('\n')), data)]

def episilon(episode):
    if episode < 20000:
        return 1 - episode * ((1 - 0.4) / 20000)
    else:
        return 0.4 - (episode - 20000) * ((0.4 - 0.025) / 80000)

if __name__ == '__main__':
    # 
    # duel = read_csv('dueling_training.csv')
    # x_dqn = np.array(range(1, 1 + len(dqn))) * 1000
    # x_duel = np.array(range(1, 1 + len(duel))) * 1000

    # epi_epi = [e for e in map(episilon, x_dqn)]

    # fig, ax1 = plt.subplots()

    # ax1.plot(x_dqn, dqn)
    # ax1.plot(x_duel, duel)
    # ax1.set_xlabel('episode')
    # ax1.set_ylabel('reward')
    # ax1.legend(labels=['DQN', 'Dueling_DQN'])

    # color = 'tab:red'
    # ax2 = ax1.twinx()
    # ax2.plot(x_dqn, epi_epi, color = color)
    # ax2.tick_params(axis='y')
    # ax2.set_ylim(0, 1)
    # ax2.legend(labels=['episilon'])
    
    # fig.tight_layout()
    
    # plt.savefig('training.png')
    dqn = read_csv('training.csv')
    x_dqn = np.array(range(1, 1 + len(dqn))) * 1000
    plt.plot(x_dqn, dqn)
    plt.xlabel('episode')
    plt.ylabel('reward')
    plt.savefig('dqn_training.png')
