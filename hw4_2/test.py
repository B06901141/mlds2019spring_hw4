"""

### NOTICE ###
You DO NOT need to upload this file

"""

import argparse
import numpy as np
from environment import Environment
import torch.multiprocessing as mp
from time import time
from tqdm import tqdm

seed = 11037

def parse():
    parser = argparse.ArgumentParser(description="MLDS 2018 HW4")
    parser.add_argument('--test_pg', action='store_true', help='whether test policy gradient')
    parser.add_argument('--test_dqn', action='store_true', help='whether test DQN')
    try:
        from argument import add_arguments
        parser = add_arguments(parser)
    except:
        pass
    args = parser.parse_args()
    return args


def test(agent, env, total_episodes=30):
    rewards = []
    from time import sleep
    infos = []
    env.seed(seed)
    for i in tqdm(range(total_episodes), ncols=80):
        state = env.reset()
        agent.init_game_setting()
        done = False
        episode_reward = 0.0
        num_step = 0
        #playing one game
        while(not done):
            # env.env.render()
            # sleep(0.1)
            action = agent.make_action(state, test=True)
            state, reward, done, info = env.step(action)
            episode_reward += reward
            num_step += 1
            if num_step > 1000:
                break
            if reward != 0:
                num_step = 0

        rewards.append(episode_reward)
    print('Run %d episodes'%(total_episodes))
    print('Mean:', np.mean(rewards))
    with open('training.csv', 'a') as fout:
        fout.write(str(np.mean(rewards)))
        fout.write('\n')

def test_v2(agent, env):

    state = env.reset()
    env.env.render(mode='rgb_array', close=True)
    agent.init_game_setting()
    done = False
    episode_reward = 0.0

    #playing one game
    while(not done):
        action = agent.make_action(state, test=True)
        state, reward, done, info = env.step(action)
        episode_reward += reward

    return episode_reward

def run(args):
    if args.test_pg:
        env = Environment('Pong-v0', args, test=True)
        from agent_dir.agent_pg import Agent_PG
        agent = Agent_PG(env, args)
        test(agent, env)

    if args.test_dqn:
        env = Environment('BreakoutNoFrameskip-v4', args, atari_wrapper=True, test=True)
        from agent_dir.agent_dqn import Agent_DQN
        agent = Agent_DQN(env, args)

        env.seed(seed)

        t = time()

        '''
        mp.set_start_method("spawn")
        with mp.Pool(processes=10) as pool:
            rewards = [pool.apply_async(test_v2, args=(agent, env)) for _ in range(10)]
            
            pool.close()
            pool.join()
        '''

        #print([r.get() for r in rewards])
        rewards = [test_v2(agent, env) for _ in range(100)]
        print(np.mean(rewards))
        print(time() - t)

if __name__ == '__main__':
    args = parse()
    run(args)
