from argparse import ArgumentParser

import cv2
import gym
import numpy as np
import torch
from tensorboardX import SummaryWriter
from torch import distributions as P
from torch import cuda, nn, optim
from torch.nn import functional as F
from torch.nn import utils as nn_utils

from model import HighWayRecurrent

device = "cuda" if cuda.is_available() else "cpu"
num_arguments = 6
model_params = {
    "sizes": [64*64*3, 64*16*3, 16*16*3, 16*16*2],
    "out_size": num_arguments,
    "n_highways": 3
}


def resize(img, size=(64, 64)):
    return cv2.resize(img, dsize=size).astype("float32")/255


def update(model, opt, OAR, gamma=.95):
    O, A, R = tuple(torch.tensor(x).to(device) for x in OAR)
    prob, V, _ = model(O)
    G = []
    nextr = 0
    for r in reversed(R):
        reward = r+nextr*gamma
        nextr = reward
        G.insert(0, reward)
    G = torch.tensor(G).float().to(device)

    assert len(O)-1 == len(G) == len(A) == len(R) == len(V)-1

    cross_entropy = 0
    for i in range(len(G)):
        cross_entropy += G[i] * F.cross_entropy(prob[i:i+1], A[i:i+1])

    loss = F.smooth_l1_loss(V[:-1].squeeze(), G) +\
        F.smooth_l1_loss(V[-1], torch.zeros(V[-1].shape).to(device)) + \
        cross_entropy
    opt.zero_grad()
    loss.backward()
    nn_utils.clip_grad_norm_(model.parameters(), 1)
    opt.step()


# if _name_ == "_main_":
#     from numpy import random
#     timesteps = 16
#     model = HighWayRecurrent(**model_params)
#     opt = optim.Adam(model.parameters(), lr=1e-3)
#     O = random.randn(timesteps+1, 64*64).astype(np.float32)
#     R = random.randn(timesteps).astype(np.float32)
#     A = random.randint(0, 7, size=[timesteps]).astype(np.int)
#     OAR = tuple(np.array(x) for x in (O, A, R))
#     update(model, opt, OAR)
#     print("end")
#     quit()


if _name_ == "_main_":
    writer = SummaryWriter()
    parser = ArgumentParser()
    parser.add_argument('-env', type=str)
    parser.add_argument('-episodes', type=int)
    flags = parser.parse_args()

    episodes = flags.episodes
    env = gym.make(flags.env)

    model = HighWayRecurrent(**model_params).to(device)
    opt = optim.Adam(model.parameters(), lr=1e-3)
    model.update = update
    sum_R = 0

    for i_episode in range(1, 1+episodes):
        print(i_episode,end=' ')
        observation = env.reset()
        observation = resize(observation).flatten()
        O = [observation]
        R = []
        A = []
        done = False
        state = None
        while not done:
            # env.render()
            observation = torch.from_numpy(observation).to(device).unsqueeze(0)
            a, v, state = model(observation, state)
            m = P.Categorical(logits=a)
            action = m.sample().cpu()
            observation, reward, done, _ = env.step(action)
            sum_R += reward
            observation = resize(observation).flatten()
            O.append(observation)
            A.append(action)
            R.append(reward)
            if reward != 0:
                OAR = tuple(np.array(x) for x in (O, A, R))
                update(model, opt, OAR)
                O = [observation]
                A = []
                R = []
                state = None
        print(sum_R)
        writer.add_scalar("reward", sum_R, i_episode)
        sum_R = 0
        torch.save(model.state_dict(), f="model.pt")