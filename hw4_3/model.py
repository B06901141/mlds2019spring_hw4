import torch
from torch import nn


class HighWay(nn.Module):
    def __init__(self, features, ns):
        super().__init__()
        self.T = nn.Linear(in_features=features, out_features=features)
        self.H = nn.Linear(in_features=features, out_features=features)
        self.leaky_relu = nn.LeakyReLU(negative_slope=ns)
        self.norm = nn.InstanceNorm1d(num_features=features)

    def forward(self, x):
        T = torch.sigmoid(self.T(x))
        H = self.leaky_relu(self.H(x))
        return self.norm((T*H+(1-T)*x).unsqueeze(0)).squeeze(0)


class HighWayRecurrent(nn.Module):
    def __init__(self, sizes, out_size, n_highways=3, ns=.2):
        super().__init__()
        hs = sizes[-1]
        self.downscale = nn.ModuleList([
            nn.Linear(in_features=i, out_features=o) for i, o in zip(sizes[:-1], sizes[1:])
        ])
        self.highway = nn.Sequential(
            *[HighWay(hs, ns) for _ in range(n_highways)]
        )
        self.rnn = nn.GRU(input_size=hs, hidden_size=hs, num_layers=1)
        self.V = nn.Linear(in_features=hs, out_features=1)
        self.A = nn.Linear(in_features=hs, out_features=out_size)

    def forward(self, x, state=None):
        for layer in self.downscale:
            x = torch.tanh(x)
            x = layer(x)
        x = self.highway(x)
        x = x.unsqueeze(0)
        x, state = self.rnn(x, state)
        x = x.squeeze(0)
        v = self.V(x)
        a = self.A(x)
        return a-v, v, state.detach()


if __name__ == "__main__":
    model = HighWayRecurrent([3, 4, 5, 6], 7)
    print(model)
    inp = torch.randn(13, 3)
    a, v, state = model(inp)
    print(a.shape, v.shape, state.shape)
